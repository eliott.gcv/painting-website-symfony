<?php
/*
namespace App\Tests;

use App\Entity\Category;
use App\Entity\Painting;
use App\Entity\User;
use DateTime;
use \DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class PaintingUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $painting = new Painting();
        $datetime = new DateTime();
        $datetimeImmutable = new DateTimeImmutable();
        $category = new Category();
        $user = new User();

        $painting->setName('name')
                ->setLenght(20.20)
                ->setHeight(20.20)
                ->setInSale(true)
                ->setRealisationDate($datetime)
                ->setCreatedAt($datetimeImmutable)
                ->setDescription('description')
                ->setPortfolio(true)
                ->setSlug('slug')
                ->setFile('file')
                ->addCategory($category)
                ->setPrice(20.20)
                ->setUser($user);

        $this->assertTrue($painting->getName() === 'name');
        $this->assertTrue($painting->getLenght() === 20.20);
        $this->assertTrue($painting->getHeight() === 20.20);
        $this->assertTrue($painting->getInSale() === true);
        $this->assertTrue($painting->getRealisationDate() === $datetime);
        $this->assertTrue($painting->getCreatedAt() === $datetimeImmutable);
        $this->assertTrue($painting->getDescription() === 'description');
        $this->assertTrue($painting->getPortfolio() === true);
        $this->assertTrue($painting->getSlug() === 'slug');
        $this->assertTrue($painting->getFile() === 'file');
        $this->assertTrue($painting->getPrice() === 20.20);
        $this->assertContains($category,$painting->getCategory());
        $this->assertTrue($painting->getUser() === $user);
    }

    public function testIsFalse()
    {
        $painting = new Painting();
        $datetime = new DateTime();
        $datetimeImmutable = new DateTimeImmutable();
        $category = new Category();
        $user = new User();

        $painting->setName('name')
            ->setLenght(20.20)
            ->setHeight(20.20)
            ->setInSale(true)
            ->setRealisationDate($datetime)
            ->setCreatedAt($datetimeImmutable)
            ->setDescription('description')
            ->setPortfolio(true)
            ->setSlug('slug')
            ->setFile('file')
            ->addCategory($category)
            ->setPrice(20.20)
            ->setUser($user);

        $this->assertFalse($painting->getName() === 'false');
        $this->assertFalse($painting->getLenght() === 20.20);
        $this->assertFalse($painting->getHeight() === 20.20);
        $this->assertFalse($painting->getInSale() === false);
        $this->assertFalse($painting->getRealisationDate() === new DateTime());
        $this->assertFalse($painting->getCreatedAt() === new DateTimeImmutable());
        $this->assertFalse($painting->getDescription() === 'false');
        $this->assertFalse($painting->getPortfolio() === false);
        $this->assertFalse($painting->getSlug() === 'false');
        $this->assertFalse($painting->getFile() === 'false');
        $this->assertFalse($painting->getPrice() === 20.20);
        $this->assertNotContains(new Category(), $painting->getCategory());
        $this->assertFalse($painting->getUser() === new User());
    }

    public function testIsEmpty()
    {
        $painting = new Painting();

        $this->assertEmpty($painting->getName() );
        $this->assertEmpty($painting->getLenght());
        $this->assertEmpty($painting->getHeight() );
        $this->assertEmpty($painting->getInSale() );
        $this->assertEmpty($painting->getRealisationDate() );
        $this->assertEmpty($painting->getCreatedAt() );
        $this->assertEmpty($painting->getDescription() );
        $this->assertEmpty($painting->getPortfolio() );
        $this->assertEmpty($painting->getSlug() );
        $this->assertEmpty($painting->getFile() );
        $this->assertEmpty($painting->getPrice() );
        $this->assertEmpty($painting->getCategory());
        $this->assertEmpty($painting->getUser() );
    }

}*/
