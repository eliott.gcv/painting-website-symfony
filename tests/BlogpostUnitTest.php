<?php

namespace App\Tests;

use App\Entity\Blogpost;
use DateTime;
use PHPUnit\Framework\TestCase;

class BlogpostUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $blogpost = new Blogpost();
        $datetime = new DateTime();

        $blogpost
            ->setTitle(('title'))
            ->setCreatedAt($datetime)
            ->setContent('content')
            ->setSlug('slug')
            ;

        $this->assertTrue($blogpost->getTitle() === 'title');
        $this->assertTrue($blogpost->getCreatedAt() === $datetime);
        $this->assertTrue($blogpost->getContent() === 'content');
        $this->assertTrue($blogpost->getSlug() === 'slug');
    }

    public function testIsFalse()
    {
        $blogpost = new Blogpost();
        $datetimeImmutable = new DateTime();

        $blogpost
            ->setTitle(('title'))
            ->setCreatedAt($datetimeImmutable)
            ->setContent('content')
            ->setSlug('slug')
            ;

        $this->assertFalse($blogpost->getTitle() === 'false');
        $this->assertFalse($blogpost->getCreatedAt() === new DateTime());
        $this->assertFalse($blogpost->getContent() === 'false');
        $this->assertFalse($blogpost->getSlug() === 'false');
    }

    public function testIsEmpty()
    {
        $blogpost = new Blogpost();

        $this->assertEmpty($blogpost->getTitle());
        $this->assertEmpty($blogpost->getCreatedAt());
        $this->assertEmpty($blogpost->getContent());
        $this->assertEmpty($blogpost->getSlug());
    }
}
