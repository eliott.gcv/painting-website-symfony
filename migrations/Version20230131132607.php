<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230131132607 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE contact (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, message LONGTEXT NOT NULL, created_at DATETIME NOT NULL, is_send TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE blogpost CHANGE created_at created_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE blogpost RENAME INDEX idx_f13ced83a76ed395 TO IDX_1284FB7DA76ED395');
        $this->addSql('DROP INDEX IDX_67F068BC418D913E ON commentaire');
        $this->addSql('ALTER TABLE commentaire CHANGE blogspot_id blogpost_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE commentaire ADD CONSTRAINT FK_67F068BC27F5416E FOREIGN KEY (blogpost_id) REFERENCES blogpost (id)');
        $this->addSql('CREATE INDEX IDX_67F068BC27F5416E ON commentaire (blogpost_id)');
        $this->addSql('ALTER TABLE peinture CHANGE created_at created_at DATETIME NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE contact');
        $this->addSql('ALTER TABLE commentaire DROP FOREIGN KEY FK_67F068BC27F5416E');
        $this->addSql('DROP INDEX IDX_67F068BC27F5416E ON commentaire');
        $this->addSql('ALTER TABLE commentaire CHANGE blogpost_id blogspot_id INT DEFAULT NULL');
        $this->addSql('CREATE INDEX IDX_67F068BC418D913E ON commentaire (blogspot_id)');
        $this->addSql('ALTER TABLE peinture CHANGE created_at created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE blogpost CHANGE created_at created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE blogpost RENAME INDEX idx_1284fb7da76ed395 TO IDX_F13CED83A76ED395');
    }
}
