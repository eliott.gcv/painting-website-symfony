<?php

namespace App\Tests;

use App\Entity\Blogpost;
use App\Entity\Commentaire;
use App\Entity\Peinture;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class CommentairesUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $commentaire = new Commentaire();
        $dateTimeImmutable = new DateTimeImmutable();
        $blogpost = new Blogpost();
        $painting = new Peinture();

        $commentaire
            ->setAuthor('author')
            ->setEmail('email@test.com')
            ->setCreatedAt($dateTimeImmutable)
            ->setContent('content')
            ->setBlogpost($blogpost)
            ->setPeinture($painting);
        $this->assertTrue($commentaire->getAuthor() === 'author');
        $this->assertTrue($commentaire->getEmail() === 'email@test.com');
        $this->assertTrue($commentaire->getCreatedAt() === $dateTimeImmutable);
        $this->assertTrue($commentaire->getContent() === 'content');
        $this->assertTrue($commentaire->getBlogpost() === $blogpost);
        $this->assertTrue($commentaire->getPeinture() === $painting);
    }

    public function testIsFalse()
    {
        $commentaire = new Commentaire();
        $dateTimeImmutable = new \DateTimeImmutable();
        $blogpost = new Blogpost();
        $painting = new Peinture();

        $commentaire
            ->setAuthor('false')
            ->setEmail('false@test.com')
            ->setCreatedAt(new DateTimeImmutable())
            ->setContent('false')
            ->setBlogpost(new Blogpost())
            ->setPeinture(new Peinture());
        $this->assertFalse($commentaire->getAuthor() === 'author');
        $this->assertFalse($commentaire->getEmail() === 'email@test.com');
        $this->assertFalse($commentaire->getCreatedAt() === $dateTimeImmutable);
        $this->assertFalse($commentaire->getContent() === 'content');
        $this->assertFalse($commentaire->getBlogpost() === $blogpost);
        $this->assertFalse($commentaire->getPeinture() === $painting);
    }

    public function testIsEmpty()
    {
        $comment = new Commentaire();

        $this->assertEmpty($comment->getAuthor());
        $this->assertEmpty($comment->getEmail());
        $this->assertEmpty($comment->getCreatedAt());
        $this->assertEmpty($comment->getContent());
        $this->assertEmpty($comment->getBlogpost());
        $this->assertEmpty($comment->getPeinture());
    }
}
