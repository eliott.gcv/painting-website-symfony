<?php

namespace App\DataFixtures;

use App\Entity\Blogpost;
use App\Entity\Categorie;
use App\Entity\Peinture;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        $user = new User();

        $user->setEmail('user@test.com')
            ->setFirstname($faker->firstName())
            ->setLastname($faker->lastName())
            ->setPhone($faker->phoneNumber())
            ->setAbout($faker->text())
            ->setSocialMedia('instagram')
            ->setPassword('')
            ->setRoles(['ROLE_PEINTRE']);

        $manager->persist($user);

        for ($i = 0; $i < 10; $i++) {
            $blogpost = new Blogpost();

            $blogpost->setTitle($faker->words(3, true))
                ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now', $timezone = null))
                ->setContent($faker->text(350))
                ->setSlug($faker->slug(3))
                ->setUser($user);

            $manager->persist($blogpost);
        }

        for ($k = 0; $k < 5; $k++) {
            $categorie = new Categorie();

            $categorie->setLastname($faker->word())
                ->setDescription($faker->words(10, true))
                ->setSlug($faker->slug());

            $manager->persist($categorie);

            for ($j = 0; $j < 2; $j++) {
                $peinture = new Peinture();

                $peinture->setName($faker->words(3, true))
                    ->setWidth($faker->randomFloat(2, 20, 60))
                    ->setHeight($faker->randomFloat(2, 20, 60))
                    ->setEnVente($faker->randomElement([true, false]))
                    ->setDateRealisation($faker->dateTimeBetween('-6 month', 'now'))
                    ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                    ->setDescription($faker->text())
                    ->setPortfolio($faker->randomElement([true, false]))
                    ->setSlug($faker->slug())
                    ->setFile('assets/img/portfolio/portfolio-1.jpg')
                    ->addCategorie($categorie)
                    ->setPrice($faker->randomFloat(2, 100, 9999))
                    ->setUser($user);

                $manager->persist($peinture);
            }
        }

        $manager->flush();
    }
}
