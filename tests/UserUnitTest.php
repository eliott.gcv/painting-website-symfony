<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\User;

class UserUnitTest extends TestCase
{
    public function testIsTrue()
    {
       $user = new User();

       $user->setEmail('true@test.com')
           ->setFirstname('firstname')
           ->setLastname('lastname')
           ->setPassword('password')
           ->setAbout('about')
           ->setSocialMedia('instagram');

       $this->assertTrue($user->getEmail() === 'true@test.com');
       $this->assertTrue($user->getFirstname() === 'firstname');
       $this->assertTrue($user->getLastname() === 'lastname');
       $this->assertTrue($user->getPassword() === 'password');
       $this->assertTrue($user->getAbout() === 'about');
       $this->assertTrue($user->getSocialMedia() === 'instagram');
    }

    public function testIsFalse()
    {
        $user = new User();

        $user->setEmail('true@test.com')
            ->setFirstname('firstname')
            ->setLastname('lastname')
            ->setPassword('password')
            ->setAbout('about')
            ->setSocialMedia('instagram');

        $this->assertFalse($user->getEmail() === 'false@test.com');
        $this->assertFalse($user->getFirstname() === 'false');
        $this->assertFalse($user->getLastname() === 'false');
        $this->assertFalse($user->getPassword() === 'false');
        $this->assertFalse($user->getAbout() === 'false');
        $this->assertFalse($user->getSocialMedia() === 'false');
    }
    public function testIsEmpty()
    {
        $user = new User();

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getLastname());
        $this->assertEmpty($user->getLastname());
        $this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getAbout());
        $this->assertEmpty($user->getSocialMedia() );
    }
}
