<?php

namespace App\Tests;

use App\Entity\Categorie;
use PHPUnit\Framework\TestCase;

class CategorieUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $categorie = new Categorie();

        $categorie->setLastname('lastname')
            ->setDescription('description')
            ->setSlug('slug');

        $this->assertTrue($categorie->getLastname() === 'lastname');
        $this->assertTrue($categorie->getDescription() === 'description');
        $this->assertTrue($categorie->getSlug() === 'slug');

    }

    public function testIsFalse()
    {
        $categorie = new Categorie();

        $categorie->setLastname('lastname')
            ->setDescription('description')
            ->setSlug('slug');

        $this->assertFalse($categorie->getLastname() === 'false');
        $this->assertFalse($categorie->getDescription() === 'false');
        $this->assertFalse($categorie->getSlug() === 'false');
    }
    public function testIsEmpty()
    {
        $categorie = new Categorie();

        $this->assertEmpty($categorie->getLastname());
        $this->assertEmpty($categorie->getDescription() );
        $this->assertEmpty($categorie->getSlug());
    }
}
